let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    'resources/assets/plantilla/css/font-awesome.css',
    'resources/assets/plantilla/css/bootstrap.css',
    'resources/assets/plantilla/css/adminlte.css',
], 'public/css/plantilla.css')
.scripts([
    'resources/assets/plantilla/js/jquery.min.js',
    'resources/assets/plantilla/js/popper.min.js',
    'resources/assets/plantilla/js/bootstrap.js',
    'resources/assets/plantilla/js/adminlte.js'
], 'public/js/plantilla.js')
.js(['resources/assets/js/app.js'], 'public/js/app.js');

